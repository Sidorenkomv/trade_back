ALTER TABLE IF EXISTS invoices_to_buyers
    ADD COLUMN IF NOT EXISTS sales_channel_id BIGINT REFERENCES sales_channel (id);
ALTER TABLE IF EXISTS invoices_to_buyers
    ADD CONSTRAINT fk_sales_channel_on_invoice_to_buyers
    FOREIGN KEY (sales_channel_id) REFERENCES sales_channel (id)
    ON DELETE SET NULL;