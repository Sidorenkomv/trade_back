CREATE SEQUENCE IF NOT EXISTS supplier_accounts_id_seq;
ALTER TABLE If EXISTS supplier_accounts
ALTER COLUMN id SET DEFAULT nextval('supplier_accounts_id_seq');
ALTER SEQUENCE supplier_accounts_id_seq OWNED BY supplier_accounts.id;