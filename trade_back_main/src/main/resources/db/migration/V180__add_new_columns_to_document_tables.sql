ALTER TABLE invoice
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_INVOICE_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE invoices_to_buyers
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_INVOICE_TO_BUYERS_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE shipments
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_SHIPMENTS_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);


ALTER TABLE supplier_accounts
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_SUPPLIER_ACCOUNTS_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);


ALTER TABLE acceptances
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_ACCEPTANCES_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE payments
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_PAYMENTS_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE internal_order
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_INTERNAL_ORDER_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE movement
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_MOVEMENT_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE inventarizations
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_INVENTARIZATIONS_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE corrections
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_CORRECTIONS_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE loss
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_LOSS_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE orders_of_production
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_ORDERS_OF_PRODUCTION_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE issued_invoices
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_ISSUED_INVOICES_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE return_suppliers
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_RETURN_SUPPLIERS_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE invoices_received
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_INVOICES_RECEIVED_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE contracts
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_CONTRACTS_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE operations
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_INVOICE_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE price_lists
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    ADD CONSTRAINT FK_INVOICE_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);