INSERT INTO supplier_accounts (id, date, company_id, warehouse_id, project_id, contract_id, contractor_id,
                               type_of_invoice, planned_date_payment, is_spend, comment, is_recyclebin, is_sent, is_print)
VALUES (1, '2022-03-13T09:03', 2, 1, 2, 1, 2, '1', '2022-05-12T09:03', false, 'privet', false, true, false),
       (2, '2022-05-17T15:23', 1, 2, 1, 2, 1, '1', '2022-06-10T09:03', false, 'privet', false, true, false),
       (3, '2022-06-05T10:40', 2, 1, 2, 1, 2, '0', '2022-07-13T09:03', false, 'privet', false, true, false),
       (4, '2022-08-08T09:03', 1, 2, 1, 2, 1, '0', '2022-08-20T09:03', false, 'privet', false, true, false),
       (5, '2022-08-10T18:43', 2, 1, 2, 1, 2, '1', '2022-09-20T09:03', false, 'privet', false, true, false);
