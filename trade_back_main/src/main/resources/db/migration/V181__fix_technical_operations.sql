ALTER TABLE technical_operations
    ADD COLUMN IF NOT EXISTS employee_id bigint default 1,
    ADD COLUMN IF NOT EXISTS last_modified_date timestamp default now(),
    DROP COLUMN IF EXISTS owner,
    DROP COLUMN IF EXISTS employee_owner,
    DROP COLUMN IF EXISTS updated_by_name,
    DROP COLUMN IF EXISTS updated_in_date,
    ADD CONSTRAINT FK_INVOICE_ON_EMPLOYEE
        FOREIGN KEY (employee_id) REFERENCES employees (id);

ALTER TABLE technical_operations
    RENAME COLUMN shared TO is_shared;
