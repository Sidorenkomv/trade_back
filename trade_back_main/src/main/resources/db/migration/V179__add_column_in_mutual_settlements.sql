ALTER TABLE mutual_settlements
    ADD COLUMN IF NOT EXISTS project_id int8,
    ADD CONSTRAINT FK_PROJECT_ON_MUTUAL_SETTLEMENTS
        FOREIGN KEY (project_id) REFERENCES projects (id);

UPDATE mutual_settlements SET project_id = 1 WHERE id = 1;
UPDATE mutual_settlements SET project_id = 2 WHERE id = 2;
UPDATE mutual_settlements SET project_id = 3 WHERE id = 3;

select * from mutual_settlements;
