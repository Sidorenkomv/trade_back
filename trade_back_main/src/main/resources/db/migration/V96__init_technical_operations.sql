INSERT INTO technical_operations (id, number, comment, is_print, is_sent, is_checked, date, is_recycle_bin,
                                  shared, updated_by_name, updated_in_date, owner, employee_owner,
                                  volume, company_id, technical_card_id, material_warehouse_id,
                                  production_warehouse_id, project_id)
VALUES (1, 30, 'Комментарий1', false, true, true,'2021-08-10 12:15:00.000000', false, false, 'Иван', '2021-08-10 12:15:00.000000', 'Иван', 'Иван', 12, 2, 2, 2, 2, 1),
       (2, 20, 'Комментарий2', false, true, true,'2021-08-10 12:15:00.000000', false, false, 'Николай', '2021-08-10 12:15:00.000000', 'Николай', 'Николай', 13, 2, 2, 2, 2, 2),
       (3, 12, 'Комментарий3', false, true, true,'2021-08-10 12:15:00.000000', false, false, 'Петр', '2021-08-10 12:15:00.000000', 'Петр', 'Петр', 14, 2, 2, 2, 2, 3);

SELECT setval('technical_operations_id_seq', max(id))
FROM technical_operations