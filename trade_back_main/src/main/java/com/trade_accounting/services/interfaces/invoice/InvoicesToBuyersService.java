package com.trade_accounting.services.interfaces.invoice;

import com.trade_accounting.models.dto.company.SupplierAccountDto;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyers;
import com.trade_accounting.models.entity.invoice.TypeOfInvoice;
import com.trade_accounting.services.interfaces.util.AbstractService;
import com.trade_accounting.services.interfaces.util.SearchableService;

import java.util.List;

public interface InvoicesToBuyersService extends AbstractService<InvoicesToBuyersDto>,
        SearchableService<InvoicesToBuyers, InvoicesToBuyersDto> {

    List<InvoicesToBuyersDto> searchByString(String nameFilter);

    List<InvoicesToBuyersDto> findBySearchAndTypeOfInvoice(String search, TypeOfInvoice typeOfInvoice);

    List<InvoicesToBuyersDto> getByProjectId(Long id);


    void moveToRecyclebin(long id);
    void restoreFromRecyclebin(long id);
}
