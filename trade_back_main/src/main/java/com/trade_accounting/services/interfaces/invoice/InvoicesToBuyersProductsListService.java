package com.trade_accounting.services.interfaces.invoice;

import com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductsListDto;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyersProductsList;
import com.trade_accounting.services.interfaces.util.AbstractService;
import com.trade_accounting.services.interfaces.util.SearchableService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface InvoicesToBuyersProductsListService extends AbstractService<InvoicesToBuyersProductsListDto>,
        SearchableService<InvoicesToBuyersProductsList, InvoicesToBuyersProductsListDto> {
    @Transactional
    default List<InvoicesToBuyersProductsListDto> searchByInvoicesId(Long id){
        return search((root, query, builder) -> builder.equal(root.get("invoicesToBuyers").get("id"), id));
    }

    @Transactional
    List<InvoicesToBuyersProductsListDto> search(Specification<InvoicesToBuyersProductsList> specification);
}
