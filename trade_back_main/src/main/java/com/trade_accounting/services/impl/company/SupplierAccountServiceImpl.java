package com.trade_accounting.services.impl.company;

import com.trade_accounting.models.dto.finance.ReturnToSupplierDto;
import com.trade_accounting.models.dto.production.OrdersOfProductionDto;
import com.trade_accounting.models.entity.company.Company;
import com.trade_accounting.models.entity.company.Contract;
import com.trade_accounting.models.entity.company.Contractor;
import com.trade_accounting.models.entity.company.SupplierAccount;
import com.trade_accounting.models.entity.finance.ReturnToSupplier;
import com.trade_accounting.models.entity.invoice.TypeOfInvoice;
import com.trade_accounting.models.entity.production.OrdersOfProduction;
import com.trade_accounting.models.entity.production.TechnicalCard;
import com.trade_accounting.models.entity.util.Project;
import com.trade_accounting.models.entity.warehouse.Warehouse;
import com.trade_accounting.models.dto.company.SupplierAccountDto;
import com.trade_accounting.repositories.client.EmployeeRepository;
import com.trade_accounting.repositories.company.CompanyRepository;
import com.trade_accounting.repositories.company.ContractRepository;
import com.trade_accounting.repositories.company.ContractorRepository;
import com.trade_accounting.repositories.company.SupplierAccountRepository;
import com.trade_accounting.repositories.util.ProjectRepository;
import com.trade_accounting.repositories.warehouse.WarehouseRepository;
import com.trade_accounting.services.interfaces.company.SupplierAccountService;
import com.trade_accounting.utils.mapper.company.CompanyMapper;
import com.trade_accounting.utils.mapper.company.ContractorMapper;
import com.trade_accounting.utils.mapper.company.SupplierAccountMapper;
import com.trade_accounting.utils.mapper.util.ProjectMapper;
import com.trade_accounting.utils.mapper.warehouse.WarehouseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class SupplierAccountServiceImpl implements SupplierAccountService {

    private final SupplierAccountRepository supplierAccountRepository;
    private final SupplierAccountMapper supplierAccountMapper;

    private final WarehouseMapper warehouseMapper;
    private final CompanyRepository companyRepository;
    private final ContractorRepository contractorRepository;
    private final ContractRepository contractRepository;
    private final WarehouseRepository warehouseRepository;
    private final EmployeeRepository employeeRepository;
    private final ProjectRepository projectRepository;
    private final CompanyMapper companyMapper;

    private final ProjectMapper projectMapper;
    private final ContractorMapper contractorMapper;

    @Override
    public List<SupplierAccountDto> getAll() {
        return supplierAccountRepository.findAll().stream().map(supplierAccountMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public SupplierAccountDto getById(Long id) {
        Optional<SupplierAccount> invoicesToCustomers = supplierAccountRepository.findById(id);
        return supplierAccountMapper.toDto(invoicesToCustomers.orElse(new SupplierAccount()));
    }

    @Override
    public List<SupplierAccountDto> getByProjectId(Long id) {
        return supplierAccountRepository.findByProjectId(id).stream()
                .map(supplierAccountMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public SupplierAccountDto create(SupplierAccountDto dto) {
        return saveOrUpdate(dto);
    }

    @Override
    public SupplierAccountDto update(SupplierAccountDto dto) {
        return saveOrUpdate(dto);
    }

    @Override
    public void deleteById(Long id) {
        supplierAccountRepository.deleteById(id);
    }


    @Override
    public List<SupplierAccountDto> searchByString(String nameFilter) {
        if (nameFilter.matches("[0-9]+")) {
            return supplierAccountRepository.searchById(Long.parseLong(nameFilter));
        } else if (nameFilter.equals("null") || nameFilter.isEmpty()) {
            return supplierAccountRepository.findAll().stream().map(supplierAccountMapper::toDto).collect(Collectors.toList());
        } else {
            return supplierAccountRepository.searchByNameFilter(nameFilter);
        }
    }


    @Override
    public List<SupplierAccountDto> search(Specification<SupplierAccount> spec) {
        return executeSearch(supplierAccountRepository, supplierAccountMapper::toDto, spec);
    }

    @Override
    public List<SupplierAccountDto> findBySearchAndTypeOfInvoice(String search, TypeOfInvoice typeOfInvoice) {
        List<SupplierAccountDto> invoiceDtoList = supplierAccountRepository.findBySearchAndTypeOfInvoice(search, typeOfInvoice);
        for (SupplierAccountDto invoice : invoiceDtoList) {
            invoice.setCompanyId(companyMapper.toDto(companyRepository.getCompaniesById(invoice.getCompanyId())).getId());
            invoice.setContractorId(contractorMapper.contractorToContractorDto(
                    contractorRepository.getOne(invoice.getContractorId())).getId());
            invoice.setWarehouseId(warehouseRepository.getById(invoice.getWarehouseId()).getId());
            invoice.setContractId(contractRepository.getById(invoice.getContractId()).getId());
            invoice.setProjectId(projectMapper.toDto(projectRepository.getProjectsById(invoice.getProjectId())).getId());
        }
        return invoiceDtoList;
    }

    @Override
    public void moveToRecyclebin(long id) {
        SupplierAccount supplierAccount = supplierAccountRepository.getOne(id);
        supplierAccount.setIsRecyclebin(true);
        supplierAccountRepository.save(supplierAccount);
    }

    @Override
    public void restoreFromRecyclebin(long id) {
        SupplierAccount supplierAccount = supplierAccountRepository.getOne(id);
        supplierAccount.setIsRecyclebin(false);
        supplierAccountRepository.save(supplierAccount);
    }

    private SupplierAccountDto saveOrUpdate(SupplierAccountDto dto) {

        SupplierAccount supplierAccount = new SupplierAccount();
        Company company = companyRepository.getCompaniesById(dto.getCompanyId());
        Warehouse warehouse = warehouseRepository.getWarehouseById(dto.getWarehouseId());
        Project project = projectRepository.getProjectsById(dto.getProjectId());
        Contract contract = contractRepository.getById(dto.getContractId());
        Contractor contractor = contractorRepository.getContractorById(dto.getContractorId());

        LocalDateTime date = LocalDateTime.parse(dto.getDate(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime plannedDatePayment = LocalDateTime.parse(dto.getPlannedDatePayment(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

        supplierAccount.setId(dto.getId());
        supplierAccount.setDate(date);
        supplierAccount.setCompany(company);
        supplierAccount.setWarehouse(warehouse);
        supplierAccount.setProject(project);
        supplierAccount.setContract(contract);
        supplierAccount.setContractor(contractor);
        supplierAccount.setTypeOfInvoice(dto.getTypeOfInvoice());
        supplierAccount.setPlannedDatePayment(plannedDatePayment);
        supplierAccount.setIsSpend(dto.getIsSpend());
        supplierAccount.setComment(dto.getComment());
        supplierAccount.setIsRecyclebin(dto.getIsRecyclebin());
        supplierAccount.setIsSent(dto.getIsSent());
        supplierAccount.setIsPrint(dto.getIsPrint());
        supplierAccount.setEmployee(employeeRepository.findById(dto.getEmployeeId()).orElse(null));
        supplierAccount.setLastModifiedDate(LocalDateTime.parse(dto.getLastModifiedDate()));

        return supplierAccountMapper.toDto(supplierAccountRepository.save(supplierAccount));

    }

}
