package com.trade_accounting.services.impl.util;

import com.trade_accounting.models.entity.util.Project;
import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.repositories.util.ProjectRepository;
import com.trade_accounting.services.interfaces.util.ProjectService;
import com.trade_accounting.utils.mapper.util.ProjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private final ProjectRepository projectRepository;
    @Autowired
    private final ProjectMapper projectMapper;

    @Override
    public List<ProjectDto> getAll() {
        List<Project> projects = projectRepository.findAll();
        List<ProjectDto> projectDtos = new ArrayList<>();
        for (Project project : projects) {
            projectDtos.add(projectMapper.toDto(project));
        }
        return projectDtos;
    }
    @Override
    public List<ProjectDto> findByArchiveFalse() {
        List<Project> projects = projectRepository.findByArchiveFalse();
        List<ProjectDto> projectDtos = new ArrayList<>();
        for (Project project : projects) {
            projectDtos.add(projectMapper.toDto(project));
        }
        return projectDtos;
    }

    @Override
    public ProjectDto getById(Long id) {
        Project project = projectRepository.findById(id).orElse(new Project());
        return projectMapper.toDto(project);
    }


    @Override
    public ProjectDto create(ProjectDto projectDto) {
        Project project = projectMapper.toModel(projectDto);
        Project projectSaved = projectRepository.save(project);
        if (projectDto.getId() == null) {
            projectDto.setId(projectSaved.getId());
        }
        return projectDto;
    }

    @Override
    public ProjectDto update(ProjectDto projectDto) {
        return create(projectDto);
    }

    @Override
    public void deleteById(Long id) {
        projectRepository.deleteById(id);
    }

    @Override
    public List<ProjectDto> searchByString(String text) {
        return projectRepository.getBySearch(text).stream()
                .map(projectMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProjectDto> search(Specification<Project> spec) {
        return executeSearch(projectRepository, projectMapper::toDto, spec);
    }
}
