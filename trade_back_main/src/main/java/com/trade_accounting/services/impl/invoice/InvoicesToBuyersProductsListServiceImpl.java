package com.trade_accounting.services.impl.invoice;


import com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductsListDto;
import com.trade_accounting.models.dto.warehouse.SupplierAccountProductsListDto;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyers;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyersProductsList;
import com.trade_accounting.models.entity.warehouse.Product;
import com.trade_accounting.models.entity.warehouse.SupplierAccountProductsList;
import com.trade_accounting.repositories.invoice.InvoicesToBuyersProductsListRepository;
import com.trade_accounting.repositories.warehouse.ProductRepository;
import com.trade_accounting.repositories.warehouse.SupplierAccountProductsListRepository;
import com.trade_accounting.services.interfaces.company.SupplierAccountService;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersProductsListService;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersService;
import com.trade_accounting.utils.mapper.company.SupplierAccountMapper;
import com.trade_accounting.utils.mapper.invoice.InvoicesToBuyersMapper;
import com.trade_accounting.utils.mapper.invoice.InvoicesToBuyersProductsListMapper;
import com.trade_accounting.utils.mapper.warehouse.SupplierAccountProductsListMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class InvoicesToBuyersProductsListServiceImpl implements InvoicesToBuyersProductsListService {

    private final InvoicesToBuyersProductsListRepository invoicesToBuyersProductsListRepository;
    private final InvoicesToBuyersProductsListMapper mapper;
    private final ProductRepository productRepository;
    private final InvoicesToBuyersService invoicesToBuyersService;
    private final InvoicesToBuyersMapper invoicesToBuyersMapper;

    @Override
    public List<InvoicesToBuyersProductsListDto> getAll() {
        return invoicesToBuyersProductsListRepository.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }

    @Override
    public InvoicesToBuyersProductsListDto getById(Long id) {
        Optional<InvoicesToBuyersProductsList> invoicesToBuyersProductsListOptional = invoicesToBuyersProductsListRepository.findById(id);
        return mapper.toDto(invoicesToBuyersProductsListOptional.orElse(new InvoicesToBuyersProductsList()));
    }

    @Override
    public InvoicesToBuyersProductsListDto create(InvoicesToBuyersProductsListDto dto) {
        return saveOrUpdate(dto);
    }

    @Override
    public InvoicesToBuyersProductsListDto update(InvoicesToBuyersProductsListDto dto) {
        return saveOrUpdate(dto);
    }

    @Override
    public void deleteById(Long id) {
        invoicesToBuyersProductsListRepository.deleteById(id);
    }

    @Override
    public List<InvoicesToBuyersProductsListDto> search(Specification<InvoicesToBuyersProductsList> specification) {
        return executeSearch(invoicesToBuyersProductsListRepository, mapper::toDto, specification);
    }

    private InvoicesToBuyersProductsListDto saveOrUpdate(InvoicesToBuyersProductsListDto dto) {
        Optional<Product> product = productRepository.findById(dto.getProductId());
        InvoicesToBuyersProductsList invoicesToBuyersProductsList = mapper.toModel(dto);
        invoicesToBuyersProductsList.setInvoicesToBuyers(invoicesToBuyersMapper.toModel(invoicesToBuyersService.getById(dto.getInvoicesToBuyersId())));
        invoicesToBuyersProductsList.setProduct(product.orElse(null));
        return mapper.toDto(invoicesToBuyersProductsListRepository.save(invoicesToBuyersProductsList));
    }

}
