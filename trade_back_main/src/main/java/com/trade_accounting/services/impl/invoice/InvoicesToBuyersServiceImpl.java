package com.trade_accounting.services.impl.invoice;

import com.trade_accounting.models.dto.company.SupplierAccountDto;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import com.trade_accounting.models.entity.company.SupplierAccount;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyers;
import com.trade_accounting.models.entity.invoice.TypeOfInvoice;
import com.trade_accounting.repositories.company.CompanyRepository;
import com.trade_accounting.repositories.company.ContractRepository;
import com.trade_accounting.repositories.company.ContractorRepository;
import com.trade_accounting.repositories.company.SupplierAccountRepository;
import com.trade_accounting.repositories.invoice.InvoicesToBuyersRepository;
import com.trade_accounting.repositories.util.ProjectRepository;
import com.trade_accounting.repositories.warehouse.WarehouseRepository;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersService;
import com.trade_accounting.utils.mapper.company.CompanyMapper;
import com.trade_accounting.utils.mapper.company.ContractorMapper;
import com.trade_accounting.utils.mapper.company.SupplierAccountMapper;
import com.trade_accounting.utils.mapper.invoice.InvoicesToBuyersMapper;
import com.trade_accounting.utils.mapper.util.ProjectMapper;
import com.trade_accounting.utils.mapper.warehouse.WarehouseMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class InvoicesToBuyersServiceImpl implements InvoicesToBuyersService {

    private final InvoicesToBuyersRepository invoicesToBuyersRepository;
    private final InvoicesToBuyersMapper invoicesToBuyersMapper;

    private final WarehouseMapper warehouseMapper;
    private final CompanyRepository companyRepository;
    private final ContractorRepository contractorRepository;
    private final ContractRepository contractRepository;
    private final WarehouseRepository warehouseRepository;
    private final ProjectRepository projectRepository;
    private final CompanyMapper companyMapper;
    private final ProjectMapper projectMapper;
    private final ContractorMapper contractorMapper;

    @Override
    public List<InvoicesToBuyersDto> getAll() {
        return invoicesToBuyersRepository.getAll();
    }

    @Override
    public InvoicesToBuyersDto getById(Long id) {
        Optional<InvoicesToBuyers> invoicesToCustomers = invoicesToBuyersRepository.findById(id);
        return invoicesToBuyersMapper.toDto(invoicesToCustomers.orElse(new InvoicesToBuyers()));
    }

    @Override
    public List<InvoicesToBuyersDto> getByProjectId(Long id) {
        return invoicesToBuyersRepository.findByProjectId(id).stream()
                .map(invoicesToBuyersMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public InvoicesToBuyersDto create(InvoicesToBuyersDto dto) {
        InvoicesToBuyers invoicesToBuyersEntity = invoicesToBuyersMapper.toModel(dto);
        return invoicesToBuyersMapper.toDto(invoicesToBuyersRepository.save(invoicesToBuyersEntity));
    }

    @Override
    public InvoicesToBuyersDto update(InvoicesToBuyersDto dto) {
        return create(dto);
    }

    @Override
    public void deleteById(Long id) {
        invoicesToBuyersRepository.deleteById(id);
    }


    @Override
    public List<InvoicesToBuyersDto> searchByString(String nameFilter) {
        if (nameFilter.matches("[0-9]+")) {
            return invoicesToBuyersRepository.searchById(Long.parseLong(nameFilter));
        } else if (nameFilter.equals("null") || nameFilter.isEmpty()) {
            return invoicesToBuyersRepository.getAll();
        } else {
            return invoicesToBuyersRepository.searchByNameFilter(nameFilter);
        }
    }

    @Override
    public List<InvoicesToBuyersDto> search(Specification<InvoicesToBuyers> spec) {
        return executeSearch(invoicesToBuyersRepository, invoicesToBuyersMapper::toDto, spec);
    }

    @Override
    public List<InvoicesToBuyersDto> findBySearchAndTypeOfInvoice(String search, TypeOfInvoice typeOfInvoice) {
        List<InvoicesToBuyersDto> invoiceDtoList = invoicesToBuyersRepository.findBySearchAndTypeOfInvoice(search, typeOfInvoice);
        for (InvoicesToBuyersDto invoice : invoiceDtoList) {
            invoice.setCompanyId(companyMapper.toDto(companyRepository.getCompaniesById(invoice.getCompanyId())).getId());
            invoice.setContractorId(contractorMapper.contractorToContractorDto(
                    contractorRepository.getOne(invoice.getContractorId())).getId());
            invoice.setWarehouseId(warehouseRepository.getById(invoice.getWarehouseId()).getId());
            invoice.setContractId(contractRepository.getById(invoice.getContractId()).getId());
            invoice.setProjectId(projectMapper.toDto(projectRepository.getProjectsById(invoice.getProjectId())).getId());
        }
        return invoiceDtoList;
    }

    @Override
    public void moveToRecyclebin(long id) {
        InvoicesToBuyers invoicesToBuyers = invoicesToBuyersRepository.getOne(id);
        invoicesToBuyers.setIsRecyclebin(true);
        invoicesToBuyersRepository.save(invoicesToBuyers);
    }

    @Override
    public void restoreFromRecyclebin(long id) {
        InvoicesToBuyers invoicesToBuyers = invoicesToBuyersRepository.getOne(id);
        invoicesToBuyers.setIsRecyclebin(false);
        invoicesToBuyersRepository.save(invoicesToBuyers);
    }
}
