package com.trade_accounting.controllers.rest.invoice;


import com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductsListDto;
import com.trade_accounting.models.dto.warehouse.SupplierAccountProductsListDto;
import com.trade_accounting.repositories.invoice.InvoicesToBuyersProductsListRepository;
import com.trade_accounting.repositories.warehouse.SupplierAccountProductsListRepository;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersProductsListService;
import com.trade_accounting.services.interfaces.util.CheckEntityService;
import com.trade_accounting.services.interfaces.warehouse.SupplierAccountProductsListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Tag(name = "Invoices To Buyers Products List Rest Controller", description = "CRUD операции с товарами в счёте покупателя")
@Api(tags = "Invoices To Buyers Products List")
@RequestMapping("/api/invoices-to-buyers-products-list")
@RequiredArgsConstructor
public class InvoicesToBuyersProductsListRestController {
    private final InvoicesToBuyersProductsListService invoicesToBuyersProductsListService;
    private final InvoicesToBuyersProductsListRepository invoicesToBuyersProductsListRepository;
    private final CheckEntityService checkEntityService;

    @ApiOperation(value = "getAll", notes = "Возвращает список всех товаров из счёта")
    @GetMapping
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка всех товаров из счёта"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<InvoicesToBuyersProductsListDto>> getAll() {
        return ResponseEntity.ok(invoicesToBuyersProductsListService.getAll());
    }

    @ApiOperation(value = "getById", notes = "Возвращает товар из счёта по Id")
    @GetMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Товар в счёте найден"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<InvoicesToBuyersProductsListDto> getById(@ApiParam(
            name = "id",
            type = "Long",
            value = "Переданный ID  в URL по которому необходимо найти товар в счёте",
            example = "1",
            required = true) @PathVariable(name = "id") Long id) {
        checkEntityService.checkExists((JpaRepository)invoicesToBuyersProductsListRepository,id);
        return ResponseEntity.ok(invoicesToBuyersProductsListService.getById(id));
    }

    @ApiOperation(value = "getByInvoicesId", notes = "Возвращает список товаров из счёта по Invoices.id")
    @GetMapping("/invoices-to-buyers-products-list/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Товар в счёте найден"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<InvoicesToBuyersProductsListDto>> getByInvoicesId(@ApiParam(
            name = "id",
            type = "Long",
            value = "Переданный ID  в URL по которому необходимо список товаров в счёте",
            example = "1",
            required = true) @PathVariable(name = "id") Long id) {
        List<InvoicesToBuyersProductsListDto> invoicesToBuyersProductsListDtos = invoicesToBuyersProductsListService.searchByInvoicesId(id);
        return ResponseEntity.ok(invoicesToBuyersProductsListDtos);
    }

    @ApiOperation(value = "create", notes = "Добавляет товар в счёт на основе переданных данных")
    @PostMapping
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Товар в счёт успешно добавлен"),
            @ApiResponse(code = 201, message = "Запрос принят и данные созданы"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> create(@ApiParam(name = "invoicesToBuyersProductsListDto",
            value = "DTO товара в счёте, который необходимо создать") @RequestBody InvoicesToBuyersProductsListDto invoicesToBuyersProductsListDto) {
        return ResponseEntity.ok().body(invoicesToBuyersProductsListService.create(invoicesToBuyersProductsListDto));
    }

    @ApiOperation(value = "update", notes = "Обновляет товар в счёте на основе переданных данных")
    @PutMapping
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Товар в счёте успешно обновлён"),
            @ApiResponse(code = 201, message = "Запрос принят и данные обновлены"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> update(@ApiParam(name = "invoicesToBuyersProductsListDto",
            value = "DTO InvoiceToBuyersListProducts, который необходимо обновить") @RequestBody InvoicesToBuyersProductsListDto invoicesToBuyersProductsListDto) {
        return ResponseEntity.ok().body(invoicesToBuyersProductsListService.update(invoicesToBuyersProductsListDto));
    }

    @ApiOperation(value = "deleteById", notes = "Удаляет товар в счёте на основе переданного ID")
    @DeleteMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Товар в счёте успешно удален"),
            @ApiResponse(code = 204, message = "Запрос получен и обработан, данных для возврата нет"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<?> deleteById(@ApiParam(
            name = "id",
            type = "Long",
            value = "Переданный ID  в URL по которому необходимо удалить товар в счёте",
            example = "1",
            required = true) @PathVariable(name = "id") Long id) {
        invoicesToBuyersProductsListService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
