package com.trade_accounting.controllers.rest.invoice;


import com.trade_accounting.models.dto.company.SupplierAccountDto;
import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import com.trade_accounting.models.entity.company.SupplierAccount;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyers;
import com.trade_accounting.repositories.invoice.InvoicesToBuyersRepository;
import com.trade_accounting.services.interfaces.invoice.InvoicesToBuyersService;
import com.trade_accounting.services.interfaces.util.CheckEntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Tag(name="InvoicesToBuyers Rest Controller", description = "CRUD операции со счетами покупателей")
@Api(tags = "InvoicesToBuyers Rest Controller")
@RequestMapping("api/invoicesToBuyers")
@RequiredArgsConstructor
public class InvoicesToBuyersRestController {
    private final InvoicesToBuyersService invoicesToBuyersService;
    private final CheckEntityService checkEntityService;
    private final InvoicesToBuyersRepository invoicesToBuyersRepository;

    @GetMapping("/search/{nameFilter}")
    @ApiOperation(value = "searchTerm", notes = "Получение списка некоторых счетов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение отф. списка контрагентов"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<InvoicesToBuyersDto>> searchByNameFilter(@ApiParam(name ="nameFilter",
            value = "Переданный в URL searchTerm, по которому необходимо найти контрагента")
                                                                       @PathVariable String nameFilter) {
        return ResponseEntity.ok(invoicesToBuyersService.searchByString(nameFilter));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "getById", notes = "Получение счета покупателя по  id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Счет покупателя найден"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<InvoicesToBuyersDto> getById(@ApiParam(name = "id", type = "Long",
            value = "Переданный в URL id, по которому необходимо найти счет покупателя")
                                                      @PathVariable(name = "id") Long id) {
        checkEntityService.checkExists((JpaRepository) invoicesToBuyersRepository, id);
        return ResponseEntity.ok(invoicesToBuyersService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "create", notes = "Добавление нового счета покупателя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Счет покупателя создан"),
            @ApiResponse(code = 201, message = "Запрос принят и счет покупателя добавлен"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<InvoicesToBuyersDto> create(@ApiParam(name = "invoice", value = "DTO счета, который необходимо создать")
                                                     @RequestBody InvoicesToBuyersDto invoice) {
        return ResponseEntity.ok(invoicesToBuyersService.create(invoice));
    }

    @PutMapping
    @ApiOperation(value = "update", notes = "Изменение счета покупателя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Информация о счете обновлена"),
            @ApiResponse(code = 201, message = "Запрос принят и счет покупателя обновлен"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<InvoicesToBuyersDto> update(@ApiParam(name = "invoice", value = "DTO счета, который необходимо создать")
                                                     @RequestBody InvoicesToBuyersDto invoice) {
        return ResponseEntity.ok(invoicesToBuyersService.update(invoice));
    }

    @ApiOperation(value = "deleteById", notes = "Удаляет счет покупателя на основе переданного ID")
    @DeleteMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Счет покупателя успешно удален"),
            @ApiResponse(code = 204, message = "Запрос получен и обработан, данных для возврата нет"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден")
    })
    public ResponseEntity<InvoicesToBuyersDto> deleteById(@ApiParam(name = "id", type = "Long",
            value = "Переданный в URL id по которому необходимо удалить счет покупателя")
                                                         @PathVariable Long id) {
        invoicesToBuyersService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/queryInvoicesToBuyers")
    @ApiOperation(value = "searchByFilter", notes = "Получение списка счетов  по заданным параметрам")
    public ResponseEntity<List<InvoicesToBuyersDto>> getAllFilter(
            @And({
                    @Spec(path = "id", params = "id", spec = Equal.class),
                    @Spec(path = "date", params = "date", spec = Equal.class),
                    @Spec(path = "contractor.name", params = "contractorDto", spec = LikeIgnoreCase.class),
                    @Spec(path = "company.name", params = "companyDto", spec = LikeIgnoreCase.class),
                    @Spec(path = "warehouse.name", params = "warehouseDto", spec = LikeIgnoreCase.class),
                    @Spec(path = "project.name", params = "projectDto", spec = LikeIgnoreCase.class),
            }) Specification<InvoicesToBuyers> invoices) {
        return ResponseEntity.ok(invoicesToBuyersService.search(invoices));
    }

    @GetMapping
    @ApiOperation(value = "getAll", notes = "Получение списка всех накладных")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка накладных"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<InvoicesToBuyersDto>> getAll() {
        return ResponseEntity.ok(invoicesToBuyersService.getAll());
    }

    @GetMapping("/getByProjectId{id}")
    @ApiOperation(value = "getByProjectId", notes = "Получение списка всех накладных по проекту")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение возвратов поставщикам"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<List<InvoicesToBuyersDto>> getByProjectId(@PathVariable Long id) {
        return ResponseEntity.ok(invoicesToBuyersService.getByProjectId(id));
    }

    @PutMapping("/moveToIsRecyclebin/{id}")
    @ApiOperation(value = "moveToIsRecyclebin", notes = "Перенос в корзину счета по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Счет перенесен в корзину"),
            @ApiResponse(code = 204, message = "Запрос получен и обработан, данных для возврата нет"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<InvoicesToBuyersDto> moveToIsRecyclebin(@ApiParam(name = "id", type = "Long",
            value = "Переданный id, по которому необходимо переместить счет")
                                                                 @PathVariable("id") Long id) {
        checkEntityService.checkExists((JpaRepository) invoicesToBuyersRepository, id);
        invoicesToBuyersService.moveToRecyclebin(id);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/restoreFromIsRecyclebin/{id}")
    @ApiOperation(value = "restoreFromIsRecyclebin", notes = "Восстановление счета по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Счет восстановлен"),
            @ApiResponse(code = 204, message = "Запрос получен и обработан, данных для возврата нет"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")}
    )
    public ResponseEntity<InvoicesToBuyersDto> restoreFromIsRecyclebin(@ApiParam(name = "id", type = "Long",
            value = "Переданный id, по которому необходимо восстановить счет")
                                                                      @PathVariable("id") Long id) {
        checkEntityService.checkExists((JpaRepository) invoicesToBuyersRepository, id);
        invoicesToBuyersService.restoreFromRecyclebin(id);
        return ResponseEntity.ok().build();
    }

}
