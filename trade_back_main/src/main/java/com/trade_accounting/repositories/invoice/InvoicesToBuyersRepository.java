package com.trade_accounting.repositories.invoice;

import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import com.trade_accounting.models.entity.invoice.TypeOfInvoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyers;

import java.util.List;

@Repository
public interface InvoicesToBuyersRepository extends JpaRepository<InvoicesToBuyers, Long>,
        JpaSpecificationExecutor<InvoicesToBuyers> {
    @Query("select new com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto (" +
            "e.id," +
            "e.date," +
            "e.company.id," +
            "e.warehouse.id," +
            "e.project.id," +
            "e.contract.id," +
            "e.contractor.id," +
            "e.typeOfInvoice," +
            "e.plannedDatePayment," +
            "e.isSpend," +
            "e.comment," +
            "e.isRecyclebin," +
            "e.isSent," +
            "e.isPrint," +
            "e.salesChannel.id," +
            "e.employee.id," +
            "e.lastModifiedDate) from InvoicesToBuyers e")
    List<InvoicesToBuyersDto> getAll();

    @Query("select new com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto (" +
            "e.id," +
            "e.date," +
            "e.company.id," +
            "e.warehouse.id," +
            "e.project.id," +
            "e.contract.id," +
            "e.contractor.id," +
            "e.typeOfInvoice," +
            "e.plannedDatePayment," +
            "e.isSpend," +
            "e.comment," +
            "e.isRecyclebin," +
            "e.isSent," +
            "e.isPrint," +
            "e.salesChannel.id," +
            "e.employee.id," +
            "e.lastModifiedDate) from InvoicesToBuyers  e where  e.id = :id")
    InvoicesToBuyersDto getById(@Param("id") Long id);

    @Query("from InvoicesToBuyers r where r.project.id = :id")
    List<InvoicesToBuyers> findByProjectId(@Param("id") Long id);

    @Query("select new com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto (" +
            "e.id," +
            "e.date," +
            "e.company.id," +
            "e.warehouse.id," +
            "e.project.id," +
            "e.contract.id," +
            "e.contractor.id," +
            "e.typeOfInvoice," +
            "e.plannedDatePayment," +
            "e.isSpend," +
            "e.comment," +
            "e.isRecyclebin," +
            "e.isSent," +
            "e.isPrint," +
            "e.salesChannel.id," +
            "e.employee.id," +
            "e.lastModifiedDate) from InvoicesToBuyers  e where  lower(e.comment) like lower(concat('%', :nameFilter,'%'))"
    )
    List<InvoicesToBuyersDto> searchByNameFilter(@Param("nameFilter") String nameFilter);

    @Query("select s from InvoicesToBuyers s where lower(concat(cast(s.id as java.lang.String), ' ', s.comment)) like lower(concat('%', :nameFilter, '%'))")
    List<InvoicesToBuyers> searchByIdAndNameFilter(@Param("nameFilter") String nameFilter);

    @Query("select new com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto (" +
            "e.id," +
            "e.date," +
            "e.company.id," +
            "e.warehouse.id," +
            "e.project.id," +
            "e.contract.id," +
            "e.contractor.id," +
            "e.typeOfInvoice," +
            "e.plannedDatePayment," +
            "e.isSpend," +
            "e.comment," +
            "e.isRecyclebin," +
            "e.isSent," +
            "e.isPrint," +
            "e.salesChannel.id," +
            "e.employee.id," +
            "e.lastModifiedDate) from InvoicesToBuyers  e where  e.id = :nameFilter "
    )
    List<InvoicesToBuyersDto> searchById(@Param("nameFilter") Long nameFilter);

    List<InvoicesToBuyers> findByTypeOfInvoice(TypeOfInvoice typeOfInvoice);

    @Query("SELECT s FROM InvoicesToBuyers s where lower(concat(s.id, s.comment, s.company.name, s.warehouse.name, s.project.name)) " +
            "like concat('%', :search, '%') and s.typeOfInvoice = :typeOfInvoice")
    List<InvoicesToBuyersDto> findBySearchAndTypeOfInvoice(@Param("search") String search,
                                                          @Param("typeOfInvoice") TypeOfInvoice typeOfInvoice);
}
