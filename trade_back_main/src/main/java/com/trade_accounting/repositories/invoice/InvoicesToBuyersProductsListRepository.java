package com.trade_accounting.repositories.invoice;

import com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductsListDto;
import com.trade_accounting.models.dto.warehouse.SupplierAccountProductsListDto;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyersProductsList;
import com.trade_accounting.models.entity.warehouse.SupplierAccountProductsList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InvoicesToBuyersProductsListRepository extends JpaRepository<InvoicesToBuyersProductsList, Long>,
        JpaSpecificationExecutor<InvoicesToBuyersProductsList> {

    @Query("select new com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductsListDto(" +
            "e.id," +
            "e.product.id," +
            "e.invoicesToBuyers.id," +
            "e.amount," +
            "e.price," +
            "e.sum," +
            "e.percentNds," +
            "e.nds," +
            "e.total) from InvoicesToBuyersProductsList e")
    List<InvoicesToBuyersProductsListDto> getAll();

    @Query("select new com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductsListDto(" +
            "e.id," +
            "e.product.id," +
            "e.invoicesToBuyers.id," +
            "e.amount," +
            "e.price," +
            "e.sum," +
            "e.percentNds," +
            "e.nds," +
            "e.total) from InvoicesToBuyersProductsList e where e.id =:id")
    List<InvoicesToBuyersProductsList> getById(@Param("id") Long id);


}
