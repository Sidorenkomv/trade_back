package com.trade_accounting.models.entity.company;



import com.trade_accounting.models.entity.client.Employee;
import com.trade_accounting.models.entity.util.Project;
import com.trade_accounting.models.entity.warehouse.Warehouse;
import com.trade_accounting.models.entity.invoice.TypeOfInvoice;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.ColumnDefault;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "supplier_accounts")
@EqualsAndHashCode
public class SupplierAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private LocalDateTime date;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse warehouse;

    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Contract contract;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Contractor contractor;

    @NotNull
    private TypeOfInvoice typeOfInvoice;//
    @NotNull
    private LocalDateTime plannedDatePayment;//
    @ColumnDefault("false")
    private Boolean isSpend;
    @NotNull
    private String comment;//
    @ColumnDefault("false")
    private Boolean isRecyclebin;
    @ColumnDefault("false")
    private Boolean isSent;//
    @ColumnDefault("false")
    private Boolean isPrint;//

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee; // Сотрудник, создавший документ

    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
}
