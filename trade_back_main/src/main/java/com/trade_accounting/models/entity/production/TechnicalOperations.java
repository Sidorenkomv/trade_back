package com.trade_accounting.models.entity.production;

import com.trade_accounting.models.entity.client.Employee;
import com.trade_accounting.models.entity.company.Company;
import com.trade_accounting.models.entity.util.Project;
import com.trade_accounting.models.entity.warehouse.Warehouse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "technical_operations")
public class TechnicalOperations {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id; // поле "айдишник"

    @Column
    private String comment; // поле "Комментарий"

    @Column
    @ColumnDefault("false")
    private Boolean isPrint = false; // поле "Напечатано"

    @Column
    @ColumnDefault("false")
    private Boolean isSent = false; // поле "Отправлено"

    @Column
    @ColumnDefault("false")
    private Boolean isChecked = false; // поле "Проверено"

    @Column
    private LocalDateTime date; // поле "Дата"

    @Column
    @ColumnDefault("false")
    private Boolean isRecycleBin = false; // поле "В корзине ли"

    @Column
    @ColumnDefault("false")
    private Boolean isShared = false; // поле "Общий доступ"

    @Column
    private Integer volume; // поле "Объем"

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company; // поле "Организация"

    @ManyToOne(fetch = FetchType.LAZY)
    private TechnicalCard technicalCard; // поле "Технологическая карта"

    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse materialWarehouse; // поле "Склад для материалов"

    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse productionWarehouse; // поле "Склад для продукции"

    @ManyToOne(fetch = FetchType.LAZY)
    private Project project; // поле "проект"

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee; // Сотрудник создавший документ

    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate; // Дата последнего изменения документа

    @Column
    private int number; // поле "номер"
}
