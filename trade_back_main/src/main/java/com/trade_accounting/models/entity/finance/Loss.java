package com.trade_accounting.models.entity.finance;


import com.trade_accounting.models.entity.client.Employee;
import com.trade_accounting.models.entity.util.OperationsAbstract;
import com.trade_accounting.models.entity.warehouse.Warehouse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "loss")
public class Loss extends OperationsAbstract {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse warehouse;

    @OneToMany(fetch = FetchType.LAZY)
    private List<LossProduct> lossProducts;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee; // Сотрудник, создавший документ

    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
}