package com.trade_accounting.models.entity.company;

import com.trade_accounting.models.entity.client.Employee;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "contracts")
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "number")
    private String number;

    @Column(name = "contract_date", columnDefinition = "date default current_date")
    private LocalDate contractDate;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY)
    private BankAccount bankAccount;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Contractor contractor;

    @Column(name = "amount")
    @ColumnDefault("0")
    private BigDecimal amount;

    @Column(name = "archive", columnDefinition = "boolean default false")
    private Boolean archive;

    @Column(name = "comment")
    private String comment;

    @OneToOne(fetch = FetchType.LAZY)
    private LegalDetail legalDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee; // Сотрудник, создавший документ

    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
}
