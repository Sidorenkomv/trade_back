package com.trade_accounting.models.entity.invoice;

import com.trade_accounting.models.entity.client.Employee;
import com.trade_accounting.models.entity.company.Company;
import com.trade_accounting.models.entity.company.Contract;
import com.trade_accounting.models.entity.company.Contractor;
import com.trade_accounting.models.entity.units.SalesChannel;
import com.trade_accounting.models.entity.util.Project;
import com.trade_accounting.models.entity.warehouse.Warehouse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "invoices_to_buyers")
@EqualsAndHashCode
public class InvoicesToBuyers {
    @Id
    @NotNull
    Long id;

    @NotNull
    private String date;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Warehouse warehouse;

    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Contract contract;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Contractor contractor;

    @NotNull
    private String typeOfInvoice;//
    @NotNull
    private String plannedDatePayment;//
    @ColumnDefault("false")
    private Boolean isSpend;
    @NotNull
    private String comment;//
    @ColumnDefault("false")
    private Boolean isRecyclebin;
    @ColumnDefault("false")
    private Boolean isSent;//
    @ColumnDefault("false")
    private Boolean isPrint;//
    @ManyToOne(fetch = FetchType.LAZY)
    private SalesChannel salesChannel;

    @ManyToOne(fetch = FetchType.LAZY)
    private Employee employee; // Сотрудник, создавший документ

    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;
}