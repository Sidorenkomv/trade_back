package com.trade_accounting.models.dto.finance;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReturnToSupplierDto {

    @NotNull
    private Long id;

    @NotNull
    private String date;

    @NotNull
    private Long warehouseId;

    @NotNull
    private Long companyId;

    @NotNull
    private Long contractorId;

    @NotNull
    private Long contractId;

    private Long projectId;

    private Boolean isSend;

    private Boolean isPrint;

    private String comment;

    private Long employeeId;

    private String lastModifiedDate;

    public ReturnToSupplierDto(Long id,
                               String date,
                               Long warehouseId,
                               Long companyId,
                               Long contractorId,
                               Long contractId,
                               Long projectId,
                               Boolean isSend,
                               Boolean isPrint,
                               String comment,
                               Long employeeId,
                               LocalDateTime lastModifiedDate) {
        this.id = id;
        this.date = date;
        this.warehouseId = warehouseId;
        this.companyId = companyId;
        this.contractorId = contractorId;
        this.contractId = contractId;
        this.projectId = projectId;
        this.isSend = isSend;
        this.isPrint = isPrint;
        this.comment = comment;
        this.employeeId = employeeId;
        this.lastModifiedDate = lastModifiedDate.toString();
    }
}
