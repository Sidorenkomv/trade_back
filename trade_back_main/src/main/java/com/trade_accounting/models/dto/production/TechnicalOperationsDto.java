package com.trade_accounting.models.dto.production;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TechnicalOperationsDto {

    private Long id;                    // поле "Номер"

    private String comment;             // поле "Комментарий"

    private Boolean isPrint = false;    // поле "Напечатано"

    private Boolean isSent = false;     // поле "Отправлено"

    private Boolean isChecked = false;  // поле "Проверено"

    private String date;                // поле "Дата"

    private Boolean isRecycleBin;       // поле "В корзине ли"

    private Boolean isShared;             // поле "Общий доступ"

    private Integer volume;             // поле "Объем"

    private Long companyId;             // поле "Организация"

    private Long technicalCardId;       // поле "Технологическая карта"

    private Long materialWarehouseId;   // поле "Склад для материалов"

    private Long productionWarehouseId; // поле "Склад для продукции"

    private Long projectId;             // поле "проект"

    private Long employeeId;            // Сотрудник создавший документ

    private String lastModifiedDate;    // Дата последнего изменения документа

    private int number;                 // поле "номер"
}
