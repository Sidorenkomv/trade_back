package com.trade_accounting.models.dto.company;

import com.trade_accounting.models.entity.invoice.TypeOfInvoice;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SupplierAccountDto {

    @NotNull
    private Long id;

    @NotNull
    private String date;

    @NotNull
    private Long companyId;

    @NotNull
    private Long warehouseId;

    private Long projectId;

    @NotNull
    private Long contractId;

    @NotNull
    private Long contractorId;

    @NotNull
    private TypeOfInvoice typeOfInvoice;

    private String plannedDatePayment;

    private Boolean isSpend;

    private String comment;

    private Boolean isRecyclebin;

    private Boolean isSent;

    private Boolean isPrint;

    private Long employeeId;

    private String lastModifiedDate;
}
