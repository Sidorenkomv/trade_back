package com.trade_accounting.models.dto.invoice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoicesToBuyersDto {

    @NotNull
    private Long id;

    @NotNull
    private String date;

    @NotNull
    private Long companyId;

    @NotNull
    private Long warehouseId;

    private Long projectId;

    public InvoicesToBuyersDto(Long id,
                               String date,
                               Long companyId,
                               Long warehouseId,
                               Long projectId,
                               Long contractId,
                               Long contractorId,
                               String typeOfInvoice,
                               String plannedDatePayment,
                               Boolean isSpend,
                               String comment,
                               Boolean isRecyclebin,
                               Boolean isSent,
                               Boolean isPrint,
                               Long salesChannelId,
                               Long employeeId,
                               LocalDateTime lastModifiedDate) {
        this.id = id;
        this.date = date;
        this.companyId = companyId;
        this.warehouseId = warehouseId;
        this.projectId = projectId;
        this.contractId = contractId;
        this.contractorId = contractorId;
        this.typeOfInvoice = typeOfInvoice;
        this.plannedDatePayment = plannedDatePayment;
        this.isSpend = isSpend;
        this.comment = comment;
        this.isRecyclebin = isRecyclebin;
        this.isSent = isSent;
        this.isPrint = isPrint;
        this.salesChannelId = salesChannelId;
        this.employeeId = employeeId;
        this.lastModifiedDate = lastModifiedDate.toString();
    }

    @NotNull
    private Long contractId;

    @NotNull
    private Long contractorId;

    @NotNull
    private String typeOfInvoice;

    private String plannedDatePayment;

    private Boolean isSpend;

    private String comment;

    private Boolean isRecyclebin;

    private Boolean isSent;

    private Boolean isPrint;

    private Long salesChannelId;

    private Long employeeId;

    private String lastModifiedDate;
}
