package com.trade_accounting.utils.mapper.finance;

import com.trade_accounting.models.entity.finance.Payment;
import com.trade_accounting.models.dto.finance.PaymentDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PaymentMapper {
    //Payment
    @Mapping(source = "companyId", target = "company.id")
    @Mapping(source = "contractorId", target = "contractor.id")
    @Mapping(source = "contractId", target = "contract.id")
    @Mapping(source = "projectId", target = "project.id")
    @Mapping(source = "date", target = "date")
    @Mapping(source = "employeeId", target = "employee.id")
    Payment toModel(PaymentDto paymentDto);


    @Mapping(source = "company.id", target = "companyId")
    @Mapping(source = "contractor.id", target = "contractorId")
    @Mapping(source = "contract.id", target = "contractId")
    @Mapping(source = "project.id", target = "projectId")
    @Mapping(source = "date", target = "date")
    @Mapping(source = "employee.id", target = "employeeId")
    PaymentDto toDto(Payment payment);
}
