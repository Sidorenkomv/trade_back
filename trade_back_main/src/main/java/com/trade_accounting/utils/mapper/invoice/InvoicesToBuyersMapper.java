package com.trade_accounting.utils.mapper.invoice;

import com.trade_accounting.models.dto.invoice.InvoicesToBuyersDto;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyers;
import com.trade_accounting.models.entity.util.Project;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface InvoicesToBuyersMapper {

    @Mapping(source = "companyId", target = "company.id")
    @Mapping(source = "contractId", target = "contract.id")
    @Mapping(source = "contractorId", target = "contractor.id")
    @Mapping(source = "warehouseId", target = "warehouse.id")
    @Mapping(source = "invoicesToBuyersDto", target = "project", qualifiedByName = "projectConverter")
    @Mapping(source = "salesChannelId", target = "salesChannel.id")
    @Mapping(source = "isSent", target = "isSent")
    @Mapping(source = "isSpend", target = "isSpend")
    @Mapping(source = "isPrint", target = "isPrint")
    @Mapping(source = "employeeId", target = "employee.id")
    InvoicesToBuyers toModel(InvoicesToBuyersDto invoicesToBuyersDto);

    @Mapping(target = "companyId", source = "company.id")
    @Mapping(target = "warehouseId", source = "warehouse.id")
    @Mapping(target = "contractId", source = "contract.id")
    @Mapping(target = "contractorId", source = "contractor.id")
    @Mapping(source = "project.id", target = "projectId")
    @Mapping(target = "salesChannelId", source = "salesChannel.id")
    @Mapping(target = "isSent", source = "isSent")
    @Mapping(target = "isSpend", source = "isSpend")
    @Mapping(target = "isPrint", source = "isPrint")
    @Mapping(target = "employeeId", source = "employee.id")
    InvoicesToBuyersDto toDto(InvoicesToBuyers invoicesToBuyers);

    @Named("projectConverter")
    default Project projectFieldFromDtoToModel(InvoicesToBuyersDto invoicesToBuyersDto) {
        return invoicesToBuyersDto.getProjectId() == null ? null :
                new Project(invoicesToBuyersDto.getProjectId(), null, null, null, false);
    }

}
