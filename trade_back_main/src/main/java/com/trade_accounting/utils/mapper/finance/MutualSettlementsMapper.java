package com.trade_accounting.utils.mapper.finance;

import com.trade_accounting.models.entity.finance.MutualSettlements;
import com.trade_accounting.models.dto.finance.MutualSettlementsDto;
import com.trade_accounting.models.entity.util.Project;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface MutualSettlementsMapper {
    //MutualSettlements
    @Mapping(source = "contractorId", target = "contractor.id")
    @Mapping(source = "employeeId", target = "employee.id")
    @Mapping(source = "mutualSettlementsDto", target = "project", qualifiedByName = "projectConverter")
    MutualSettlements toModel(MutualSettlementsDto mutualSettlementsDto);

    @Mapping(source = "contractor.id", target = "contractorId")
    @Mapping(source = "employee.id", target = "employeeId")
    @Mapping(source = "project.id", target = "projectId")
    MutualSettlementsDto toDto(MutualSettlements mutualSettlements);

    @Named("projectConverter")
    default Project projectFieldFromDtoToModel(MutualSettlementsDto mutualSettlementsDto) {
        return mutualSettlementsDto.getProjectId() == null ? null :
                new Project(mutualSettlementsDto.getProjectId(), null, null, null, null);
    }

}
