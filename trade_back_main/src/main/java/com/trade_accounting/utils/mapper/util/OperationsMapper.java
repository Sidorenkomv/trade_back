package com.trade_accounting.utils.mapper.util;

import com.trade_accounting.models.entity.util.OperationsAbstract;
import com.trade_accounting.models.dto.util.OperationsDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Optional;

@Mapper(componentModel = "spring")
public interface OperationsMapper {
    //Operations
    @Mapping(target = "companyId", source = "company.id")
    @Mapping(target = "employeeId", source = "employee.id")
    OperationsDto toDto(OperationsAbstract operationsAbstract);
}


