package com.trade_accounting.utils.mapper.finance;

import com.trade_accounting.models.entity.finance.Loss;
import com.trade_accounting.models.entity.finance.LossProduct;
import com.trade_accounting.models.dto.finance.LossDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface LossMapper {
    //Loss
    @Mapping(source = "warehouseId", target = "warehouse.id")
    @Mapping(source = "companyId", target = "company.id")
    @Mapping(source = "lossProductsIds", target = "lossProducts")
    @Mapping(source = "employeeId", target = "employee.id")
    Loss toModel(LossDto lossDto);

    @Mapping(target = "warehouseId", source = "warehouse.id")
    @Mapping(target = "companyId", source = "company.id")
    @Mapping(target = "lossProductsIds", source = "lossProducts")
    @Mapping(target = "employeeId", source = "employee.id")
    LossDto toDto(Loss loss);

    default Long lossProductToLong(LossProduct lossProducts) {
        return lossProducts.getId();
    }

    default LossProduct longToLossProduct(Long id) {
        return LossProduct.builder()
                .id(id)
                .build();
    }
}
