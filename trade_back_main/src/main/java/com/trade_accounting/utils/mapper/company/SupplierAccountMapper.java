package com.trade_accounting.utils.mapper.company;

import com.trade_accounting.models.dto.finance.ReturnToSupplierDto;
import com.trade_accounting.models.entity.company.Company;
import com.trade_accounting.models.entity.company.Contract;
import com.trade_accounting.models.entity.company.Contractor;
import com.trade_accounting.models.entity.company.SupplierAccount;
import com.trade_accounting.models.entity.finance.ReturnToSupplier;
import com.trade_accounting.models.entity.invoice.TypeOfInvoice;
import com.trade_accounting.models.entity.util.Project;
import com.trade_accounting.models.entity.warehouse.Warehouse;
import com.trade_accounting.models.dto.company.SupplierAccountDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Mapper(componentModel = "spring")
public interface SupplierAccountMapper {
    //Supplier
//    @Mapping(target = "date", ignore = true)
   @Mapping(source = "companyId", target = "company.id")
   @Mapping(source = "contractId", target = "contract.id")
   @Mapping(source = "contractorId", target = "contractor.id")
   @Mapping(source = "warehouseId", target = "warehouse.id")
   @Mapping(target = "date", source = "date", dateFormat = "dd-MM-yyyy HH:mm")
   @Mapping(target = "plannedDatePayment", source = "plannedDatePayment", dateFormat = "dd-MM-yyyy HH:mm")
   @Mapping(source = "supplierAccountDto", target = "project", qualifiedByName = "projectConverter")
   @Mapping(source = "employeeId", target = "employee.id")
   SupplierAccount toModel(SupplierAccountDto supplierAccountDto);

    @Mapping(target = "companyId", source = "company.id")
    @Mapping(target = "warehouseId", source = "warehouse.id")
    @Mapping(target = "contractId", source = "contract.id")
    @Mapping(target = "contractorId", source = "contractor.id")
    @Mapping(source = "project.id", target = "projectId")
    @Mapping(target = "date", source = "date", dateFormat = "dd-MM-yyyy HH:mm")
    @Mapping(target = "plannedDatePayment", source = "plannedDatePayment", dateFormat = "dd-MM-yyyy HH:mm")
    @Mapping(target = "employeeId", source = "employee.id")
    SupplierAccountDto toDto(SupplierAccount supplierAccount);
    @Named("projectConverter")
    default Project projectFieldFromDtoToModel(SupplierAccountDto supplierAccountDto) {
        return supplierAccountDto.getProjectId() == null ? null :
                new Project(supplierAccountDto.getProjectId(), null, null, null, false);
    }
}
