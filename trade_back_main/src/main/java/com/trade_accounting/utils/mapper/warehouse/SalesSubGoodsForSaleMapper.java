package com.trade_accounting.utils.mapper.warehouse;

import com.trade_accounting.models.dto.warehouse.SalesSubGoodsForSaleDto;
import com.trade_accounting.models.entity.warehouse.SalesSubGoodsForSale;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SalesSubGoodsForSaleMapper {
    //SalesSubGoodsForSale
    @Mapping(source = "productId", target = "name.id")
    SalesSubGoodsForSale toModel(SalesSubGoodsForSaleDto salesSubGoodsForSaleDto);

    @Mapping(target = "productId", source = "name.id")
    SalesSubGoodsForSaleDto toDto(SalesSubGoodsForSale salesSubGoodsForSale);
}
