package com.trade_accounting.utils.mapper.production;

import com.trade_accounting.models.dto.production.TechnicalOperationsDto;
import com.trade_accounting.models.entity.production.TechnicalOperations;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TechnicalOperationsMapper {
    //TechnicalOperations
    @Mapping(source = "date", target = "date", dateFormat = "yyyy-MM-dd HH:mm")
    @Mapping(target = "company.id", source = "companyId")
    @Mapping(target = "technicalCard.id", source = "technicalCardId")
    @Mapping(target = "materialWarehouse.id", source = "materialWarehouseId")
    @Mapping(target = "productionWarehouse.id", source = "productionWarehouseId")
    @Mapping(target = "project.id", source = "projectId")
    @Mapping(target = "employee.id", source = "employeeId")
    TechnicalOperations toModel(TechnicalOperationsDto technicalOperationsDto);


    @Mapping(target = "companyId", source = "company.id")
    @Mapping(target = "technicalCardId", source = "technicalCard.id")
    @Mapping(target = "materialWarehouseId", source = "materialWarehouse.id")
    @Mapping(target = "productionWarehouseId", source = "productionWarehouse.id")
    @Mapping(source = "date", target = "date", dateFormat = "yyyy-MM-dd HH:mm")
    @Mapping(target = "projectId", source = "project.id")
    @Mapping(target = "employeeId", source = "employee.id")
    TechnicalOperationsDto toDto(TechnicalOperations technicalOperations);

}
