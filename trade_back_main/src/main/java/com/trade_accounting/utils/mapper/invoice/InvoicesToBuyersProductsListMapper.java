package com.trade_accounting.utils.mapper.invoice;

import com.trade_accounting.models.dto.invoice.InvoicesToBuyersProductsListDto;
import com.trade_accounting.models.dto.warehouse.SupplierAccountProductsListDto;
import com.trade_accounting.models.entity.invoice.InvoicesToBuyersProductsList;
import com.trade_accounting.models.entity.warehouse.SupplierAccountProductsList;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface InvoicesToBuyersProductsListMapper {

    @Mapping(source = "invoicesToBuyersId", target = "invoicesToBuyers.id")
    @Mapping(source = "productId", target = "product.id")
    InvoicesToBuyersProductsList toModel(InvoicesToBuyersProductsListDto invoicesToBuyersProductsListDto);

    @Mapping(source = "invoicesToBuyers.id", target = "invoicesToBuyersId")
    @Mapping(source = "product.id", target = "productId")
    InvoicesToBuyersProductsListDto toDto(InvoicesToBuyersProductsList invoicesToBuyersProductsList);

}
