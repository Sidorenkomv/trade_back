TRUNCATE technical_cards CASCADE;
TRUNCATE warehouses CASCADE;
TRUNCATE companies CASCADE;
TRUNCATE technical_operations CASCADE;
TRUNCATE projects CASCADE;
TRUNCATE employees CASCADE;

INSERT INTO technical_cards (id, comment, name, production_cost, technical_card_group_id)
VALUES (1, 'Комментарий 1', 'Техническая карта №1', '1000', 1);

INSERT INTO warehouses(id, name, sort_number, address, comment_to_address, comment)
VALUES (1, 'name1', 'sort num 1', 'address1', 'comment to addr 1', 'comment1');

INSERT INTO companies(id, name, inn, sort_number, phone, fax, email, payer_vat, comment_to_address, leader,
                      leader_manager_position, leader_signature, chief_accountant, chief_accountant_signature,
                      stamp)
VALUES (1, 'OOO ���� �1', '1234', '0001', '+79436527610', '810-41-1234567823', 'veraogon@mail.ru', true,
        'something comment', 'testLeader', 'testLeaderMeneger', 'testLeaderSignature', 'chiefTest',
        'chiefTestAccount', 'stampTest');

INSERT INTO projects (id, code, description, name)
VALUES (1, '0001', 'description1', 'name1');

INSERT INTO employees (id, description, email, first_name, inn, last_name, middle_name, password, phone, sort_number,
                       department_id, image_id, position_id)
VALUES (1, 'Some special text about Vasya', 'vasyaogon@mail.ru', 'Vasya', '526317984689', 'Vasiliev', 'Vasilievich',
        '12345', '+7(999)111-22-33', '1', 1, 1, 1);

INSERT INTO technical_operations (id, number, comment, is_print, is_sent, is_checked, date, is_recycle_bin, is_shared, volume, company_id,
                                  technical_card_id, material_warehouse_id, production_warehouse_id, project_id, is_recyclebin, employee_id, last_modified_date)
VALUES (1, 30, 'Comment1', false, false, false, '2021-08-10 12:15:00.000000', false, false, 12, 1, 1, 1, 1, 1, false, 1, '2021-08-10 12:15:00.000000'),
       (2, 20, 'Comment2', false, false, false, '2021-08-10 12:15:00.000000', false, false, 12, 1, 1, 1, 1, 1, false, 1, '2021-08-10 12:15:00.000000'),
       (3, 12, 'Comment3', false, false, false, '2021-08-10 12:15:00.000000', false, false, 12, 1, 1, 1, 1, 1, false, 1, '2021-08-10 12:15:00.000000');