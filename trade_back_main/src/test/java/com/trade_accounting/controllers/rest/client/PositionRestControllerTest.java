package com.trade_accounting.controllers.rest.client;

import com.google.gson.Gson;
import com.trade_accounting.models.dto.client.PositionDto;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Stanislav Dusiak
 * @since 06.08.2021
 */

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"spring.config.location = src/test/resources/application-test.yml"})
@Sql(value = "/datasets/client/Position-before.sql")
@WithUserDetails(value = "vasyaogon@mail.ru")
@RequiredArgsConstructor
@AutoConfigureRestDocs(outputDir = "target/snippets", uriScheme = "http", uriPort = 4444)
public class PositionRestControllerTest {

    @Autowired
    private PositionRestController positionRestController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testExistence() {
        assertNotNull(positionRestController, "Position Rest Controller is null");
    }

    @Test
    public void getAll() throws Exception {
        mockMvc.perform(get("/api/position"))
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(jsonPath("$", hasSize(4)))
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));
    }

    @Test
    public void getById() throws Exception {
        String PositionDtoJson = new Gson().toJson(PositionDto.builder()
                .id(1L)
                .name("Vasya")
                .sortNumber("1")
                .build());

        mockMvc.perform(get("/api/position/1"))
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().json(PositionDtoJson))
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));
    }

    @Test
    public void create() throws Exception {
        String PositionDtoJson = new Gson().toJson(PositionDto.builder()
                .id(1L)
                .name("Vasya")
                .sortNumber("2")
                .build()
        );

        mockMvc.perform(post("/api/position")
                        .contentType(MediaType.APPLICATION_JSON).content(PositionDtoJson))
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().json(PositionDtoJson))
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));

        mockMvc.perform(get("/api/position"))
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    public void update() throws Exception {
        String PositionDtoJson = new Gson().toJson(PositionDto.builder()
                .id(1L)
                .name("Vasya")
                .sortNumber("2")
                .build()
        );

        mockMvc.perform(put("/api/position")
                        .contentType(MediaType.APPLICATION_JSON).content(PositionDtoJson))
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().json(PositionDtoJson))
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));
        mockMvc.perform(get("/api/position"));
    }

    @Test
    public void deleteById() throws Exception {
        mockMvc.perform(delete("/api/position/2"))
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));
        mockMvc.perform(get("/api/position"))
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(jsonPath("$", hasSize(3)));
    }
}