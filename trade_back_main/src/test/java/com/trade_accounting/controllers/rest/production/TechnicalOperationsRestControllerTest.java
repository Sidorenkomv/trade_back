package com.trade_accounting.controllers.rest.production;

import com.google.gson.Gson;
import com.trade_accounting.models.dto.production.TechnicalOperationsDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"spring.config.location = src/test/resources/application-test.yml"})
@Sql(scripts = "classpath:technicalOperations-before.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@WithUserDetails(value = "vasyaogon@mail.ru")
@AutoConfigureRestDocs(outputDir = "target/snippets", uriScheme = "http", uriPort = 4444)
public class TechnicalOperationsRestControllerTest {

    @Autowired
    private TechnicalOperationsRestController technicalOperationsRestController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testExistence() {
        assertNotNull(technicalOperationsRestController, "ProductionTargets Rest Controller is null");
    }

    @Test
    void testGetAll() throws Exception {
        mockMvc.perform(get("/api/technical/operations"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(jsonPath("$", hasSize(3)))
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));
    }

    @Test
    void testGetById () throws Exception {
        String modelJson = new Gson().toJson(TechnicalOperationsDto.builder()
                .id(1L)
                .number(30)
                .comment("Comment1")
                .isPrint(false)
                .isSent(false)
                .isChecked(false)
                .date("2021-08-10 12:15")
                .isRecycleBin(false)
                .isShared(false)
                .volume(12)
                .companyId(1L)
                .technicalCardId(1L)
                .materialWarehouseId(1L)
                .productionWarehouseId(1L)
                .projectId(1L)
                .employeeId(1L)
                .lastModifiedDate("2021-08-10T12:15:00")
                .build());

        mockMvc.perform(get("/api/technical/operations/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().json(modelJson))
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));
    }

    @Test
    void testCreate () throws Exception {
        String modelJson = new Gson().toJson(TechnicalOperationsDto.builder()
                .number(30)
                .comment("Comment4 (create)")
                .isPrint(false)
                .isSent(false)
                .isChecked(false)
                .date("2021-08-10 12:15")
                .isRecycleBin(false)
                .isShared(false)
                .volume(12)
                .companyId(1L)
                .technicalCardId(1L)
                .materialWarehouseId(1L)
                .productionWarehouseId(1L)
                .projectId(1L)
                .employeeId(1L)
                .lastModifiedDate("2021-08-10T12:15:00")
                .build());

        mockMvc.perform(post("/api/technical/operations")
                        .contentType(MediaType.APPLICATION_JSON).content(modelJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().json(modelJson))
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));

        mockMvc.perform(get("/api/technical/operations"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    void testUpdate() throws Exception {
        String modelJson = new Gson().toJson(TechnicalOperationsDto.builder()
                .id(1L)
                .number(30)
                .comment("Comment4 (update)")
                .isPrint(false)
                .isSent(false)
                .isChecked(false)
                .date("2021-08-10 12:15")
                .isRecycleBin(false)
                .isShared(false)
                .volume(12)
                .companyId(1L)
                .technicalCardId(1L)
                .materialWarehouseId(1L)
                .productionWarehouseId(1L)
                .projectId(1L)
                .employeeId(1L)
                .lastModifiedDate("2021-08-10T12:15:00")
                .build());

        mockMvc.perform(put("/api/technical/operations")
                        .contentType(MediaType.APPLICATION_JSON).content(modelJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(content().json(modelJson))
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));

        mockMvc.perform(get("/api/technical/operations"))
                .andDo(print());
    }

    @Test
    void testDeleteById() throws Exception {
        mockMvc.perform(delete("/api/technical/operations/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}"));

        mockMvc.perform(get("/api/technical/operations"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(authenticated())
                .andExpect(jsonPath("$", hasSize(2)));
    }
}
