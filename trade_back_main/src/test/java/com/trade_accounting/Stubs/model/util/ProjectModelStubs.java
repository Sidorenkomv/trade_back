package com.trade_accounting.Stubs.model.util;

import com.trade_accounting.models.dto.util.DiscountDto;
import com.trade_accounting.models.dto.util.ProjectDto;
import com.trade_accounting.models.entity.util.Discount;
import com.trade_accounting.models.entity.util.Project;

public class ProjectModelStubs {
    public static Project getProject(Long id) {
        return Project.builder()
                .id(id)
                .name("name "+id)
                .code("000"+id)
                .description("description: "+id)
                .archive(false)
                .build();
    }

    public static ProjectDto getProjectDto(Long id) {
        return new ProjectDto(id, "name "+id,
                "000"+id, "description: "+id, false);
    }
}
