package com.trade_accounting.Stubs.model.warehouse;

import com.trade_accounting.Stubs.model.util.ProjectModelStubs;
import com.trade_accounting.models.entity.warehouse.Movement;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.trade_accounting.Stubs.ModelStubs.*;
import static com.trade_accounting.Stubs.model.warehouse.MovementProductModelStubs.getMovementProduct;

public class MovementModelStubs {
    public static Movement getMovement(Long id) {
        return Movement.builder()

                .id(id)
                .movementProducts(new ArrayList<>())
                .project(ProjectModelStubs.getProject(1L))
                .warehouse(getWarehouse(1L))
                .comment("Комментарий " + id)
                .date(LocalDateTime.now())
                .build();
    }
}
